import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Sudoku {
    public static void main(String[] args) {
        long start, end;

        for (String filename : args) {
            try {
                System.out.println(filename);
                start = System.currentTimeMillis();
                Board b = new Parser().parse(new Scanner(new File(filename)));
                b.solve();
                end = System.currentTimeMillis();

                System.out.printf("time = %dms\n", (end - start));
                System.out.println();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
